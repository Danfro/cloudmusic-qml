��    C      4  Y   L      �     �     �     �     �     �     �               !     '     ?     F      O     p     w          �     �     �     �     �     �     �     �  
   �     �     �     �               1     E     M     Y     f  $   k  f   �     �  
   �                    '  	   0     :     @  *   N     y     �     �     �     �     �     �     �     �     �      	     	     	     !	     7	  	   C	     M	  
   Y	     d	  |  p	     �
     �
          .     >     G     \     p     ~     �     �     �  (   �     �     �     �     �                         5     D     L     `     m     �     �     �     �     �     �     �     	       ,     j   K     �     �     �     �     �     �     �     
       +   !     M     U     h     n  +   v     �     �     �     �     �     �               "     <  	   S  
   ]     h     t                C   ;          0                  4       1   3      6   $      5   9                ?                                  &          '   #   ,       <   @          B   %   .                   "   -   7   (       A            /   8      2      =   !              )   +      *           >      	   
              :    %1 song %1 songs %1 songs played today 0 songs played today About Add Add Playlist Add to playlist Add to queue Album Album added to playlist Albums Ambiance An error occurred
Touch to retry Artist Artists Available offline Cancel Close Cloud Music Create Create new playlist Credits Delete Delete playlist Developers Donate Download Download quality Edit playlist Edit playlist name Enter playlist name Extreme Go to album Go to artist High I'm sorry but I forgot that lyric :( I'm sorry, list is empty because none of the songs included in this album are of a supported format :( Icon New Albums New playlist Normal Now Playing Playlist Playlists Queue Release Date: Released under the terms of the GNU GPL v3 Remove Report bugs on %1 Save Search Select one playlist to add Settings Song added to playlist Song added to queue Song removed Songs Streaming quality SuruDark Theme This cannot be undone Top Artists Top Songs Translators Version %1 What's new? Project-Id-Version: cloudmusic
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-05-13 18:03+0200
Last-Translator: Gabriele <Unknown>
Language-Team: Italian <it@li.org>
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2017-05-06 05:51+0000
X-Generator: Poedit 2.3
 %1 brano %1 brani %1 brani ascoltati oggi 0 brani ascoltati oggi Informazioni su Aggiungi Aggiungi Lista brani Aggiungi alla Lista Metti in coda Album Album aggiunto alla playlist Album Ambiance C'é stato un'errore
Premi per riprovare Artista Artisti Disponibile offline Cancella Chiudi Cloud Music Crea Crea nuova Lista brani Riconoscimenti Elimina Elimina lista brani Sviluppatori Effettua una donazione Scarica Qualità di scaricamento Modifica Lista brani Modifica nome alla lista brani Inserire nome Lista brani Estremo Vai all'album Vai all'artista Alto Mi dispiace ma ne ho dimenticato il testo :( Mi dispiace, la lista è vuota perché nessun brano incluso in questo album è di un formato supportato :( Icona Nuovi album Nuova Lista brani Normale In riproduzione Lista brani Liste brani Coda Data di rilascio: Rilasciato sotto i termini della GNU GPL v3 Rimuovi Riporta i bug a %1 Salva Ricerca Seleziona una Lista brani su cui aggiungere Impostazioni Brano aggiunto alla playlist Brano aggiunto alla coda Canzone rimossa Brani Qualità del flusso dati SuruDark Tema Non può essere annullato Artisti più ascoltati Top brani Traduttori Versione %1 Che c'é di nuovo? 